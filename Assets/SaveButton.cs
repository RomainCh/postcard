﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class SaveButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{

    bool active;
    [SerializeField] float activeScale=1.1f;
    [SerializeField] float inactiveScale=1f;
    [SerializeField] float scaleSpeed=10;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float scale = transform.localScale.x;

        if(active)
        {
            scale = Mathf.Min(scale + scaleSpeed * Time.deltaTime, activeScale);
            transform.localScale = new Vector3(scale, scale, scale);
        }
        else
        {
            scale = Mathf.Max(scale - scaleSpeed * Time.deltaTime, inactiveScale);
            transform.localScale = new Vector3(scale, scale, scale);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        active = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        active = true;
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        Saver.Save();
    }
}
