﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightIndicator : MonoBehaviour
{

    [SerializeField] GameObject light;

    // Start is called before the first frame update
    void Start()
    {
        foreach(Location location in GetComponentsInChildren<Location>())
        {
            GameObject newLight = Instantiate(light);
            newLight.GetComponent<Light>().color = new Color(Random.Range(0.5f, 1.0f), Random.Range(0.5f, 1f), Random.Range(0.5f, 1f), 1);
            newLight.transform.position = location.transform.position;
            newLight.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
