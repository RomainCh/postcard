﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class ItemUI : MonoBehaviour, IPointerEnterHandler,IPointerExitHandler
{

    [SerializeField]
    Item item;
    [SerializeField]
    Text itemName;
    bool selected = false;

    [SerializeField]
    TextAsset inkAsset;

    Image image;
    void Awake()
    {
        image = GetComponent<Image>();
    }
    void Update()
    {
        if(selected)
        {
            Vector3 mousePos = Input.mousePosition;
            itemName.transform.position = new Vector3(mousePos.x, mousePos.y, mousePos.z);
            if(Input.GetMouseButtonDown(1))
            {
                //Description
                //StartDialog.LoadDialog(item.description);
                DialogDisplayer.instance.BeginDialog(inkAsset, "items." + DialogDisplayer.FormatStringForInk(item.nom));
            }
            if (Input.GetMouseButtonDown(0))
            {
                Controller.item = item;
                Cursor.SetCursor(item.cursorTexture, Vector2.zero, CursorMode.Auto);
            }
        }
    }
    // Start is called before the first frame update
    public void OnPointerEnter(PointerEventData eventData)
    {
        itemName.gameObject.SetActive(true);
        itemName.text = item.nom;
        itemName.transform.position = Input.mousePosition;
        selected = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        itemName.gameObject.SetActive(false);
        selected = false;
    }

    public void UpdateItem(Item item)
    {
        if(item != null)
        {
            this.item = item;
            image.sprite = item.sprite;
            image.enabled = true;
        }
        else
        {
            this.item = null;
            image.enabled = false;
        }
        
    }
}
