﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    [SerializeField]
    float active_y;
    [SerializeField]
    float inactive_y;
    [SerializeField]
    float y_speed;

    [SerializeField]
    GameObject board;



    bool active=false;

    // Start is called before the first frame update
    void Start()
    {
        UpdateUI();
    }

    void UpdateUI()
    {
        ItemUI[] itemUIs = GetComponentsInChildren<ItemUI>();

        
        foreach (ItemUI itemUI in itemUIs)
        {
            itemUI.UpdateItem(null);
        }
        int i = 0;
        foreach (Item item in ScenarioManager.Inventory.Items.Values)
        {
            if (i < itemUIs.Length)
            {
                itemUIs[i].UpdateItem(item);
                i++;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

        float dy = Time.deltaTime * y_speed;
        if (active)
        {
            
            UpdateUI();
            board.SetActive(true);
            Vector2 pos = transform.localPosition;
            if (pos.y>active_y)
            {
                pos=new Vector2(pos.x, Mathf.Max(pos.y - dy, active_y));
            }
            transform.localPosition = pos;
        }
        else
        {
            Vector2 pos = transform.localPosition;
            if (pos.y < inactive_y)
            {
                pos = new Vector2(pos.x, Mathf.Min(pos.y + dy, inactive_y));
            }
            transform.localPosition = pos;

            if(pos.y==inactive_y)
            {
                board.SetActive(false);
            }
        }


        if(Input.GetAxis("Mouse ScrollWheel") >0)
        {
            Inactivate();
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            Activate();
        }

        Controller.inventoryOpen = active;

    }

    void Activate()
    {
        active = true;
    }
    void Inactivate()
    {
        active = false;
    }

}
