﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Inventory
{
    [SerializeField]
    Dictionary<string, Item> items= new Dictionary<string, Item>();//To get an item through its name


    public Dictionary<string, Item> Items
    {
        get
        {
            return items;
        }
    }
    public void AddItem(Item item)
    {
        if(!items.ContainsKey(item.nom))
        {
            items.Add(item.nom, item);
        }
    }

    public bool AddItem(string itemName)
    {
        if (!allItems.ContainsKey(itemName))
        {
            Debug.LogError("Item \"" + itemName + "\"not found");
            return false;
        }
        Item item = allItems[itemName];
        items[itemName] =  item;
        Debug.Log("Added " + itemName);
        return true;
    }

    public bool HasItem(string itemName)
    {
        if (!allItems.ContainsKey(itemName))
        {
            Debug.LogError("Item \"" + itemName + "\"not found");
        }
        return items.ContainsKey(itemName);
    }
    public bool HasItem(Item item)
    {
        return HasItem(item.nom);
    }

    public void DeleteItem(string itemName)
    {
        if (!allItems.ContainsKey(itemName))
        {
            Debug.LogError("Item \"" + itemName + "\"not found");
        }
        if(Controller.item != null && Controller.item.nom==itemName)
        {
            Controller.ResetItem();
        }
        items.Remove(itemName);
    }
    public void DeleteItem(Item item)
    {
        DeleteItem(item.nom);
    }


    //To get an item through its name
    public static Dictionary<string, Item> allItems = new Dictionary<string, Item>();

    public Inventory()
    {

    }
}

