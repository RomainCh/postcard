﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Item", menuName ="Item")]
public class Item : ScriptableObject
{
    [SerializeField]
    public string nom;

    [TextArea(5,20)]
    [SerializeField]
    public string description;

    [SerializeField]
    public Sprite sprite;

    [SerializeField]
    public Texture2D cursorTexture;

}
