﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller: MonoBehaviour
{
    public static Item item;
    public static bool inventoryOpen;
    public static bool dialogOpen;

    [SerializeField] Texture2D defaultCursor;

    public static Texture2D baseCursor;

    void Start()
    {
        baseCursor = defaultCursor;
        Cursor.SetCursor(baseCursor, Vector2.zero, CursorMode.Auto);
    }

    void Update()
    {
        if(Input.GetMouseButtonUp(1))
        {
            ResetItem();
        }
    }

    public static bool WorldMouseButtonDown(int i)
    {
        return !inventoryOpen && !dialogOpen && Input.GetMouseButtonDown(i);
    }

    public static bool MouseAvailableWorld()
    {
        return !inventoryOpen && !dialogOpen;
    }

    public static void ResetItem()
    {
        item = null;
        //Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        Cursor.SetCursor(baseCursor, Vector2.zero, CursorMode.Auto);
        
    }

}
