﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Dessine une fonction en utilisant un line renderer
/// </summary>
public class LineDrawing : Drawing
{
    private LineRenderer _lineRenderer;
    [SerializeField] private int _nbAbsc;

    private float xMin;
    public float XMin
    {
        get
        {
            return xMin;
        }
        private set
        {
            xMin = value;
        }
    }

    private float xMax;
    public float XMax
    {
        get
        {
            return xMax;
        }
        private set
        {
            xMax = value;
        }
    }

    void Awake()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        Debug.Assert(_nbAbsc > 1);
        Debug.Assert(_lineRenderer != null);
        
        xMin = _lineRenderer.GetPosition(0).x;
        xMax = _lineRenderer.GetPosition(_lineRenderer.positionCount - 1).x;
        _lineRenderer.positionCount = _nbAbsc;
        CurrentFunction = (x => 0f);
    }

    /*private void LateUpdate()
    {
        DrawFunc(CurrentFunction);
    }*/

    /// <summary>
    /// Dessine la fonction passée en paramètre sur le line renderer
    /// </summary>
    /// <param name="function"></param>
    protected override void DrawFunc(Func function)
    {
        Vector3[] positions = new Vector3[_nbAbsc];

        for (int i = 0; i < _nbAbsc; ++i)
        {
            float x = Mathf.Lerp(xMin, xMax, (float)i / (float)(_nbAbsc - 1));
            positions[i] = new Vector3(x, function(x), 0f);
        }

        _lineRenderer.SetPositions(positions);

    }
}
