﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Drawing : MonoBehaviour
{
    /// <summary>
    /// Encapsulation de la fonction à dessiner
    /// </summary>
    /// <param name="xValue">Abscisse</param>
    /// <returns>Ordonnée</returns>
    public delegate float Func(float xValue);

    /// <summary>
    /// Fonction actuellement dessinée
    /// </summary>
    protected Func _currentFunction;
    public Func CurrentFunction
    {
        get
        {
            return _currentFunction;
        }
        set
        {
            _currentFunction = value;
            DrawFunc(value);
        }
    }

    private void Awake()
    {
        CurrentFunction = Mathf.Sin;
    }

    /// <summary>
    /// Dessine la fonction passée en paramètre
    /// </summary>
    /// <param name="function"></param>
    protected abstract void DrawFunc(Func function);
}
