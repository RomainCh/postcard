﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

/// <summary>
/// Classe pour dessiner une somme de sinusoïdes paramétrables à l'aide de boutons
/// </summary>
[RequireComponent(typeof(Drawing))]
public class FunctionParam : MonoBehaviour
{
    #region Subclasses

    [Serializable]
    private class Bounds
    {
        public float minAmp, maxAmp, minFreq, maxFreq, minOffset, maxOffset;
        public int nbChoice;
    }

    /// <summary>
    /// Classe pour stocker les paramètres d'une sinusoïde
    /// </summary>
    [Serializable]
    private class FunctionPresets
    {
        [SerializeField, Range(-5, 5f)] public float magnitude;

        [SerializeField, Range(0.1f, 1.5f)] public float frequency;

        [SerializeField, Range(0, 360)] public float offset;
    }

    /// <summary>
    /// Classe pour gérer une fonction paramétrable
    /// </summary>
    [Serializable]
    private class Function
    {
        /// <summary>
        /// Les presets de la fonction
        /// </summary>
        [Tooltip("Les presets de la fonction")]
        public FunctionPresets[] presets;
        /// <summary>
        /// Le texte où est écrit le numéro de la fonction et le preset choisi
        /// </summary>
        [Tooltip("Le texte où est écrit le numéro de la fonction et le preset choisi")]
        public Text text;
        /// <summary>
        /// Le preset utilisé actuellement
        /// </summary>
        [Tooltip("Le preset utilisé actuellement")]
        public int currentPreset;
    }

    #endregion

    /// <summary>
    /// Liste des fonctions paramétrables
    /// </summary>
    [SerializeField] private Function[] _functions;

    /// <summary>
    /// L'outil de dessin de fonction
    /// </summary>
    private Drawing _fd;

    [SerializeField] private Bounds _bounds;

    /// <summary>
    /// Incrémente le preset utilisé par la fonction numéro index cycliquement
    /// </summary>
    /// <param name="index">L'index de la fonction cible</param>
    public void Increment(int index)
    {
        if (index < _functions.Length && index >= 0)
        {
            ++_functions[index].currentPreset;
            _functions[index].currentPreset %= _functions[index].presets.Length;
            _functions[index].text.text = "Piste " + (index + 1) + " : " + (_functions[index].currentPreset + 1); 
        }

        _fd.CurrentFunction = FunctionSum;
    }

    /// <summary>
    /// Décrémente le preset utilisé par la fonction numéro index cycliquement
    /// </summary>
    /// <param name="index">Index de la fonction cible</param>
    public void Decrement(int index)
    {
        if (index < _functions.Length && index >= 0)
        {
            --_functions[index].currentPreset;
            _functions[index].currentPreset = (_functions[index].currentPreset % _functions[index].presets.Length + _functions[index].presets.Length) % _functions[index].presets.Length;
            _functions[index].text.text = "Piste " + (index + 1) + " : " + (_functions[index].currentPreset + 1);
        }

        _fd.CurrentFunction = FunctionSum;
    }

    private void Awake()
    {
        _fd = GetComponent<Drawing>();
        InitPresets();
        Debug.Assert(_bounds.nbChoice > 1);
    }

    private void Start()
    {
        _fd.CurrentFunction = FunctionSum;
    }

    #region Presets Generation

    /// <summary>
    /// Créer les presets des fonctions de manières aléatoires, sans avoir 2 preset identique
    /// </summary>
    private void InitPresets()
    {
        float[] freqs = new float[_functions.Length * _bounds.nbChoice];

        for (int i = 0; i < freqs.Length; ++i)
        {
            freqs[i] = UnityEngine.Random.Range(_bounds.minFreq + i * (_bounds.maxFreq - _bounds.minFreq) / freqs.Length, _bounds.minFreq + (i+1) * (_bounds.maxFreq - _bounds.minFreq) / freqs.Length);
        }
        KnuthShuffle(freqs);

        for (int i = 0; i < _functions.Length; ++i)
        {
            _functions[i].presets = new FunctionPresets[_bounds.nbChoice];
            for (int j = 0; j < _bounds.nbChoice; ++j)
            {
                GenPreset(i, j, freqs);
            }
        }
    }

    /// <summary>
    /// Mélange l'array en paramètre
    /// </summary>
    /// <param name="freqs"></param>
    private void KnuthShuffle(float[] freqs)
    {
        // Knuth shuffle algorithm
        for (int t = 0; t < freqs.Length; t++)
        {
            float tmp = freqs[t];
            int r = UnityEngine.Random.Range(t, freqs.Length);
            freqs[t] = freqs[r];
            freqs[r] = tmp;
        }
    }

    /// <summary>
    /// Génération d'un preset
    /// </summary>
    /// <param name="i">Le numéro de la fonction</param>
    /// <param name="j">Le numéro du preset dans cette fonction</param>
    /// <param name="freqs">Les fréquences aléatoires sélectionnées</param>
    private void GenPreset(int i, int j, float[] freqs)
    {
        Debug.Assert(i < _functions.Length && j < _bounds.nbChoice);

        FunctionPresets preset = new FunctionPresets
        {
            frequency = freqs[i * _bounds.nbChoice + j],
            magnitude = UnityEngine.Random.Range(_bounds.minAmp, _bounds.maxAmp),
            offset = 0// UnityEngine.Random.Range(_bounds.minOffset, _bounds.maxOffset);
        };
        _functions[i].presets[j] = preset;
    }

    #endregion

    /// <summary>
    /// Calcule la somme des fonctions paramétrées
    /// </summary>
    /// <param name="x">L'abscisse</param>
    /// <returns>L'ordonnée</returns>
    private float FunctionSum(float x)
    {
        float res = 0f;

        foreach(Function func in _functions)
        {
            FunctionPresets preset = func.presets[func.currentPreset];
            res += preset.magnitude * Mathf.Sin(x * preset.frequency + preset.offset * Mathf.PI / 180f);
        }

        return res;
    }

    #region Public Soluce

    /// <summary>
    /// Génération aléatoire de solution
    /// </summary>
    /// <returns></returns>
    public int[] GenSoluce()
    {
        int[] res = new int[_functions.Length];
        int count = 0; //nombre de non-zéro

        for (int i = 0; i < _functions.Length; ++i)
        {
            int choice = UnityEngine.Random.Range(0, _functions.Length - i);
            if (choice < 2 - count)
            {
                res[i] = UnityEngine.Random.Range(1, _bounds.nbChoice);
                ++count;
            }
            else
            {
                res[i] = UnityEngine.Random.Range(0, _bounds.nbChoice);

            }
            
        }

        return res;
    }

    /// <summary>
    /// Vérification de la solution avec les presets actuels
    /// </summary>
    /// <param name="soluce"></param>
    /// <returns></returns>
    public bool CheckSoluce(int[] soluce)
    {
        Debug.Assert(soluce.Length == _functions.Length, "Problème de solution au check");

        for (int i = 0; i < _functions.Length; ++i)
        {
            if (soluce[i] != _functions[i].currentPreset) return false;
        }
        return true;
    }

    public Drawing.Func Soluce(int[] soluce)
    {
        Debug.Assert(soluce.Length == _functions.Length, "Problème de solution au gen de fonction soluce");
        
        //Inner function to return a float -> float function
        float SoluceFunction(float x)
        {
            float res = 0f;
            for (int i = 0; i < _functions.Length; ++i)
            {
                FunctionPresets preset = _functions[i].presets[soluce[i]];
                res += preset.magnitude * Mathf.Sin(x * preset.frequency + preset.offset * Mathf.PI / 180f);
            }
            return res;
        }

        return SoluceFunction;
    }

    #endregion
}
