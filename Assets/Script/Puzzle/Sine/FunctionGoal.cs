﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FunctionGoal : PuzzleManager
{
    /// <summary>
    /// La fonction paramétrable manipulée par le joueur
    /// </summary>
    [SerializeField] private FunctionParam _functionParam;
    /// <summary>
    /// La fonction cible dessinée
    /// </summary>
    [SerializeField] private Drawing _functionDrawing;
    /// <summary>
    /// Array d'index pour les presets de la fonction paramétrable
    /// </summary>
    private int[] _soluce = { 0 };

    new protected void Start()
    {
        base.Start();
        _soluce = _soluce = _functionParam.GenSoluce();
        _functionDrawing.CurrentFunction = _functionParam.Soluce(_soluce);
    }

    private void Update()
    {
        //TODO Debug reload
        if (Input.GetKeyDown(KeyCode.R)) SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Check()
    {
        if (_functionParam.CheckSoluce(_soluce))
        {
            LineRenderer lr = _functionParam.GetComponent<LineRenderer>();
            if (lr != null)
            {
                lr.startColor = Color.green;
                lr.endColor = Color.green;
            }
            LoadBack(0);
        }
    }
}
