﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Dessine une fonction en utilisant un particle system
/// </summary>
public class FunctionDrawing : Drawing
{
    /// <summary>
    /// Le particle system sur lequel on dessine
    /// </summary>
    private ParticleSystem _particleSystem;
    /// <summary>
    /// Stockage des particules dans un tableau
    /// </summary>
    private ParticleSystem.Particle[] _particles;


    // Start is called before the first frame update
    void Awake()
    {
        _particleSystem = GetComponent<ParticleSystem>();
        _particles = new ParticleSystem.Particle[_particleSystem.main.maxParticles];
    }

    void LateUpdate()
    {
        DrawFunc(CurrentFunction);
    }

    /// <summary>
    /// Dessine la fonction passée en paramètre sur le particle system
    /// </summary>
    /// <param name="function"></param>
    protected override void DrawFunc(Func function)
    {
        //Particles alive
        int numParticles = _particleSystem.GetParticles(_particles);

        for (int i = 0; i < numParticles; ++i)
        {
            // Set particles local y according to its local x using function
            Vector3 newPos = _particles[i].position + _particles[i].velocity * Time.deltaTime;
            _particles[i].position = new Vector3(newPos.x, function(newPos.x), newPos.z);
        }

        _particleSystem.SetParticles(_particles, numParticles);
    }
}
