﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Gère la position de la pièce par rapport à la cible.
/// </summary>
[RequireComponent(typeof(DragHandler))]
public class DragTarget : MonoBehaviour
{
    [Tooltip("Position cible.")]
    [SerializeField] private Transform target;
    [Tooltip("Distance minimale avec la cible pour se fixer.")]
    [SerializeField] private float minDistance;
    private PuzzleGoal goal;

    SpriteRenderer spriteRenderer;


    //Internal
    private DragHandler drag;
    private new BoxCollider2D collider;

    private void Awake()
    {
        drag = GetComponent<DragHandler>();
        collider = GetComponent<BoxCollider2D>();
        goal = FindObjectOfType<PuzzleGoal>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnMouseUp()
    {
        if (Vector2.Distance(transform.position, target.transform.position) < minDistance)
        {
            transform.position = new Vector3(target.position.x, target.position.y, 0f);
            drag.enabled = false;
            collider.enabled = false;
            ++goal.CurrPlaced;
            --spriteRenderer.sortingOrder;
        }
    }
}
