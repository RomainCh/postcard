﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleGoal : PuzzleManager
{

    [SerializeField] private Transform _targetTransform;

    private int _targets;
    private int _currPlaced = 0;
    public int CurrPlaced
    {
        get
        {
            return _currPlaced;
        }
        set
        {
            _currPlaced = value;
        }
    }

    private void Awake()
    {
        _targets = _targetTransform.childCount;
    }

    private void Update()
    {
        if (_currPlaced >= _targets)
        {
            LoadBack(0);
        }
    }
}
