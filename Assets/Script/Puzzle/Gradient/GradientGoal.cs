﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GradientGoal : PuzzleManager
{
    [SerializeField] private SimpleMoving _sphere;
    [SerializeField] private float _failCooldown;
    private float _timeSinceFail;

    private void Update()
    {
        _timeSinceFail += Time.deltaTime;
        if (Input.GetMouseButtonDown(0) && _timeSinceFail > _failCooldown)
        {
            if (_sphere.Check())
            {
                LoadBack(0);
            }
            else
            {
                _timeSinceFail = 0f;
            }
        }
        //TODO Debug reload
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

}
