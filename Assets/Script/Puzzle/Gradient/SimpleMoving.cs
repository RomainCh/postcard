﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMoving : MonoBehaviour
{
    [SerializeField] private LineDrawing _lineDrawing;
    [SerializeField] private Color _threshold;
    [SerializeField, Range(0.5f, 3f)] private float _speed = 2f;
    private Gradient _gradient;
    private LineRenderer _lineRenderer;

    // Start is called before the first frame update
    void Start()
    {
        _lineRenderer = _lineDrawing.GetComponent<LineRenderer>();
        _gradient = _lineRenderer.colorGradient;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = Mathf.Sin(_speed * Time.time) * 7f * transform.parent.right;
        /*transform.localPosition = (transform.localPosition.x + _speed * Time.deltaTime) * transform.parent.right;
        if (transform.localPosition.x < _lineDrawing.XMin || transform.localPosition.x > _lineDrawing.XMax)
        {
            _speed *= -1;
        }*/
    }

    public bool Check()
    {
        float time = (transform.localPosition.x - _lineDrawing.XMin) / (_lineDrawing.XMax - _lineDrawing.XMin);
        Color pick = _lineRenderer.colorGradient.Evaluate(time);
        Debug.Log(time);
        return pick.g > _threshold.g && pick.r == 1;
    }
}
