﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GradientRandomizer : MonoBehaviour
{
    [SerializeField, Range(float.Epsilon, 10f)] private float _difficulty;

    [SerializeField, Range(0f, 100f)] private float _thresholdMin;
    [SerializeField, Range(0f, 100f)] private float _thresholdMax;

    [SerializeField, Range(0f, 100f)] private float _positionMin;
    [SerializeField, Range(0f, 100f)] private float _positionMax;

    private LineRenderer _lineRenderer;
    private Gradient _gradient;

    private void Awake()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        Debug.Assert(_lineRenderer != null);
        _gradient = _lineRenderer.colorGradient;
        Debug.Assert(_gradient.colorKeys.Length == 5, "Gradient set up error");
    }

    // Start is called before the first frame update
    void Start()
    {
        RandomGradient();
    }

    private void RandomGradient()
    {
        //float threshold = Random.Range(25f, 45f) / 100f;
        float x = Random.Range(0f, 1f);

        float threshold = (_thresholdMin + (_thresholdMax - _thresholdMin) * (1f - Mathf.Pow(Random.Range(0f, 1f), _difficulty))) / 100f;
        float position = (_positionMin + (_positionMax - _positionMin) * Random.Range(0f, 1f)) / 100f;

        GradientColorKey[] colorKeys = _gradient.colorKeys;
        GradientAlphaKey[] alphaKeys = _gradient.alphaKeys;

        if (position - threshold <= 0f)
        {
            colorKeys[0].color = colorKeys[2].color;
            colorKeys[1].color = colorKeys[2].color;
        }
        if (position + threshold > 1f)
        {
            colorKeys[4].color = colorKeys[2].color;
            colorKeys[3].color = colorKeys[2].color;
        }
        colorKeys[1].time = Mathf.Clamp(position - threshold, 0f, 100f);
        colorKeys[2].time = position;
        colorKeys[3].time = Mathf.Clamp(position + threshold, 0f, 100f);

        _gradient.SetKeys(colorKeys, alphaKeys);

        _lineRenderer.colorGradient = _gradient;
    }
}
