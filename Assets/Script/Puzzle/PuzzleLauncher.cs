﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PuzzleLauncher : MonoBehaviour
{

    [SerializeField] string puzzle;
    private void OnMouseUp()
    {
        Launch();
    }
    public void Launch()
    {
        StartCoroutine(Load(puzzle));
    }
    private IEnumerator Load(string sceneName)
    {
        //animator.SetBool("fade", true);
        yield return new WaitForSeconds(.75f);
        SceneManager.LoadScene(sceneName);
    }
}
