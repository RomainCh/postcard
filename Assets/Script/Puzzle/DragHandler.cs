﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Rend un objet déplaçable à la souris sur le plan xy.
/// </summary>
[RequireComponent(typeof(Collider2D))]
public class DragHandler : MonoBehaviour
{
    private Vector2 startPos;
    private bool isHeld = false;

    private void Update()
    {
        if (isHeld == true)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(mousePos.x - startPos.x, mousePos.y - startPos.y, transform.position.z);
        }
    }

    private void OnMouseDown()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            isHeld = true;

            startPos = mousePos - transform.position;
        }
    }

    private void OnMouseUp()
    {
        isHeld = false;
    }
}
