﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


/// <summary>
/// Manage les puzzles.
/// </summary>
public abstract class PuzzleManager : MonoBehaviour
{
    [Tooltip("Background image")]
    [SerializeField] private Image background;

    protected void Start()
    {
        Debug.Log(LocationManager.currentLocation.place);
        //background.sprite = LocationManager.currentLocation.sprite;
        //background.sprite = LocationManager.currentSprite;
        background.sprite = Resources.Load<Sprite>("Locations/" + LocationManager.currentLocation.place);
        
        Debug.Log("Loaded Background");
    }

    /// <summary>
    /// Load back to main scene with return score.
    /// </summary>
    /// <param name="score">Score of the puzzle.</param>
    public void LoadBack(int score)
    {
        ScenarioManager.HorrorLevel += score;
        StartCoroutine(Load());
    }

    private IEnumerator Load()
    {
        //animator.SetBool("fade", false);
        yield return new WaitForSeconds(.75f);

        SceneManager.LoadScene(LocationManager.scene);
    }
}
