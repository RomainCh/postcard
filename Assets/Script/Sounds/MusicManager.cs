﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    static MusicManager _Instance;
    public static MusicManager Instance
    {
        get
        {
            if (_Instance == null) { Debug.LogError("You're missing a MusicManager"); }
            return _Instance;
        }
        private set { _Instance = value; }
    }

    private AudioSource musicSource;

    public enum Music { Stop = -1, Normal, Prelude, Enigma, Revelation, Tension, Elevator};
    private Music currentMusic = Music.Normal;

    [NamedArray(typeof(Music))]
    [SerializeField] List<AudioClip> musicClips;

    void Awake()
    {
        if (_Instance == null) {
            Instance = this;
        }
        else if (Instance != this) {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);

        musicSource = this.GetComponent<AudioSource>();
    }

    //BGM________________________________________________________________________
    public void StopBGM()
    {
        musicSource.Stop();
    }

    public void PlayBGM(Music music)
    {
        if(music == Music.Stop) {
            StopBGM();
            return;
        }
        if (music == currentMusic) { return; }

        musicSource.loop = true;
        musicSource.clip = musicClips[(int) music];
        currentMusic = music;
        musicSource.Play();
    }
    public void PlayBGM(string musicString)
    {
        Music music;
        if (System.Enum.TryParse<Music>(musicString, out music))
        {
            PlayBGM(music);
        }
        else
        {
            Debug.LogError("Invalid music: " + musicString);
        }
    }
}
