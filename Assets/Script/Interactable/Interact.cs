﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ink;
using UnityEngine.UI;
public class Interact : MonoBehaviour
{


    [SerializeField]
    TextAsset inkAsset;

    [SerializeField]
    string id;

    [SerializeField]
    string indication;

    [SerializeField]
    Text indicationText;

    Location location;


    void Start()
    {

        location = transform.parent.GetComponentInChildren<Location>();
        if(indicationText==null)
        {
            indicationText = DialogDisplayer.instance.mouseText;
        }
        if(indication=="")
        {
            indication = id;
        }

        if(indicationText==null)
        {
            indicationText = DialogDisplayer.instance.mouseText;
        }
    }
    public void OnMouseOver()
    {

        if (IsEnabled() && Controller.MouseAvailableWorld())
        {
            indicationText.text = indication;
            indicationText.gameObject.SetActive(true);
            Vector3 mousePos = Input.mousePosition;
            indicationText.transform.position = new Vector3(mousePos.x, mousePos.y, mousePos.z);

            if (Controller.WorldMouseButtonDown(0))
            {
                DialogDisplayer.instance.BeginInteraction(inkAsset, id);
            }
        }

    }

    public void OnMouseExit()
    {
        indicationText.gameObject.SetActive(false);
    }
    
    bool IsEnabled()
    {
        return LocationManager.currentLocation.place == location.place;
    }

}
