﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System;

public enum Flag
{
    VISITEDCYBERPUNK,
    VISITEDTRAMWAY,
    VISITEDMANOIR,

    FIRSTDIALOG,
    SECONDDIALOG,
    THIRDDIALOG,

    CHEATEDONCE,

    MADEPEACE
}
public struct FlagNames
{
    public static string[] FlagStrings =
    {
    "VISITEDCYBERPUNK",
    "VISITEDTRAMWAY",
    "VISITEDMANOIR",

    "FIRSTDIALOG",
    "SECONDDIALOG",
    "THIRDDIALOG",

    "CHEATEDONCE",

    "MADEPEACE"
    };
}

public class ScenarioManager
{
    public static int saveNumber;

    static int horrorLevel;
    static Dictionary<string, float> anger = new Dictionary<string, float >();
    public static Dictionary<string, bool> flags = new Dictionary<string, bool>();
    static Inventory inventory = new Inventory();

    public static Inventory Inventory
    {
        get
        {
            return inventory;
        }
        set
        {
            inventory = value;
            Saver.SaveMeta();
        }
    }
    public static Dictionary<string, float> Anger
    {
        get
        {
            return anger;
        }
        set
        {
            anger = value;
            Saver.SaveMeta();
        }
    }
    public static void RaiseFlag(Flag flag)
    {
        flags[FlagNames.FlagStrings[(int)flag]] = true;
        Saver.SaveMeta();
    }

    public static void DropFlag(Flag flag)
    {
        flags[FlagNames.FlagStrings[(int)flag]] = false;
        Saver.SaveMeta();
    }

    public static void RaiseFlag(string flagString)
    {
        Flag flag;
        if (Enum.TryParse<Flag>(flagString, out flag))
        {
            RaiseFlag(flag);
        }
        else
        {
            Debug.LogError("Invalid flag: " + flagString);
        }
        Saver.SaveMeta();
    }

    public static void DropFlag(string flagString)
    {
        Flag flag;
        if(Enum.TryParse<Flag>(flagString, out flag))
        {
            DropFlag(flag);
        }
        else
        {
            Debug.LogError("Invalid flag: " + flagString);
        }
        Saver.SaveMeta();
    }

    public static bool IsRaised(Flag flag)
    {
        return flags.ContainsKey(FlagNames.FlagStrings[(int)flag]) 
            && flags[FlagNames.FlagStrings[(int)flag]];
    }
    public static bool IsRaised(string flagString)
    {
        Flag flag;
        if (Enum.TryParse<Flag>(flagString, out flag))
        {
            return IsRaised(flag);
        }
        else
        {
            Debug.LogError("Invalid flag: " + flagString);
            return false;
        }
    }

    public static int HorrorLevel
    {
        get
        {
            return horrorLevel;
        }
        set
        {
            horrorLevel = value;
            Saver.SaveMeta();
        }
    }


   
}

[Serializable]
public class ScenarioData
{
    public string[] ids;
    public float[] angers;
    public string[] flagStrings;
    public bool[] flagStatuses;
    public string[] items;
    public bool meta;
    
    [SerializeField] public Place savePlace;

    public int horrorLevel;

    public ScenarioData()
    {
        //Anger
        List<string> idsList = ScenarioManager.Anger.Keys.ToList();
        int angerNum = idsList.Count;
        ids = new string[angerNum];
        angers = new float[angerNum];
        for(int i=0;i< angerNum;i++)
        {
            ids[i] = idsList[i];
            angers[i] = ScenarioManager.Anger[ids[i]];
        }

        //Flag
        List<string> flagsList = ScenarioManager.flags.Keys.ToList();
        int flagsNum = flagsList.Count;
        flagStrings = new string[flagsNum];
        flagStatuses = new bool[flagsNum];
        for (int i = 0; i < flagsNum; i++)
        {
            flagStrings[i] = flagsList[i];
            flagStatuses[i] = ScenarioManager.flags[flagStrings[i]];
        }

        //Place
        if(LocationManager.currentLocation!=null)
        {
            savePlace = LocationManager.currentLocation.place;
        }
        

        //HorrorLevel
        horrorLevel = ScenarioManager.HorrorLevel;

        //Inventory
        List<string> itemList = ScenarioManager.Inventory.Items.Keys.ToList();
        int itemsNum = itemList.Count;
        items = new string[itemsNum];
        for (int i = 0; i < itemsNum; i++)
        {
            items[i] = itemList[i];
        }
    }


    

}
