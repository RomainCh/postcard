﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
public class Saver
{

    public static void Save()
    {
        string path = Path.Combine(Application.persistentDataPath, "Scenario" + ScenarioManager.saveNumber + "-save.sav");
        FileStream stream = new FileStream(path, FileMode.Create);

        ScenarioData data = new ScenarioData();
        data.meta = false;
        BinaryFormatter formatter = new BinaryFormatter();
        formatter.Serialize(stream, data);

        Debug.Log("Saved");

        stream.Close();
        SaveMeta();
    }

    public static void SaveMeta()
    {
        
        string path = Path.Combine(Application.persistentDataPath, "Scenario-Meta" + ScenarioManager.saveNumber + "-save.sav");
        FileStream stream = new FileStream(path, FileMode.Create);

        ScenarioData data = new ScenarioData();
        data.meta = true;

        BinaryFormatter formatter = new BinaryFormatter();
        formatter.Serialize(stream, data);

        //Debug.Log("SavedMeta");
        stream.Close();
    }

    public static void Load(int saveNumber)
    {
        ScenarioManager.saveNumber = saveNumber;
        Load();
    }
        public static void Load()
    {
        bool cheated=false;
        //Save
        string path = Path.Combine(Application.persistentDataPath, "Scenario" + ScenarioManager.saveNumber + "-save.sav");

        if (File.Exists(path))
        {
            FileStream stream = new FileStream(path, FileMode.Open);
            BinaryFormatter formatter = new BinaryFormatter();
            ScenarioData data = (ScenarioData)formatter.Deserialize(stream);

            

            Dictionary<string, float> anger = new Dictionary<string, float>();
            Dictionary<string, bool> flags = new Dictionary<string, bool>();

            Inventory inventory = new Inventory();

            //Anger
            for (int i = 0; i < Mathf.Min(data.ids.Length, data.angers.Length); i++)
            {
                anger[data.ids[i]] = data.angers[i];
            }
            //Flags
            for (int i = 0; i < Mathf.Min(data.flagStrings.Length, data.flagStatuses.Length); i++)
            {
                flags[data.flagStrings[i]] = data.flagStatuses[i];
            }


            //Inventory
            foreach (string itemName in data.items)
            {
                inventory.AddItem(itemName);
            }



            //Meta
            string pathMeta = Path.Combine(Application.persistentDataPath, "Scenario-Meta" + ScenarioManager.saveNumber + "-save.sav");

            if (File.Exists(pathMeta))
            {
                FileStream streamMeta = new FileStream(pathMeta, FileMode.Open);
                Debug.Log(pathMeta);
                BinaryFormatter formatterMeta = new BinaryFormatter();
                ScenarioData dataMeta = (ScenarioData)formatterMeta.Deserialize(streamMeta);

                Dictionary<string, float> angerMeta = new Dictionary<string, float>();
                Dictionary<string, bool> flagsMeta = new Dictionary<string, bool>();
                Inventory inventoryMeta = new Inventory();
                if(dataMeta.meta)
                {
                    //AngerMeta
                    for (int i = 0; i < Mathf.Min(dataMeta.ids.Length, dataMeta.angers.Length); i++)
                    {
                        angerMeta[dataMeta.ids[i]] = dataMeta.angers[i];
                    }
                    //FlagsMeta
                    for (int i = 0; i < Mathf.Min(dataMeta.flagStrings.Length, dataMeta.flagStatuses.Length); i++)
                    {
                        flagsMeta[dataMeta.flagStrings[i]] = dataMeta.flagStatuses[i];
                    }

                    //Inventory
                    foreach (string itemName in dataMeta.items)
                    {
                        inventoryMeta.AddItem(itemName);
                    }


                    //Comparison
                    //Flags
                    foreach (string s in flagsMeta.Keys)
                    {
                        Debug.Log(s + "/(Meta)" + flagsMeta[s]);
                    }

                    foreach (string s in flagsMeta.Keys)
                    {
                        if (!flags.ContainsKey(s) || flags[s] != flagsMeta[s])
                        {
                            Debug.LogWarning("Cheated on " + s);
                            cheated = true;
                        }
                    }
                    //Inventory 
                    foreach(string itemName in inventory.Items.Keys)
                    {
                        if(!inventoryMeta.HasItem(itemName))
                        {
                            Debug.LogWarning("Cheated on used item " + itemName);
                            cheated = true;
                        }
                    }
                    foreach (string itemName in inventoryMeta.Items.Keys)
                    {
                        if (!inventory.HasItem(itemName))
                        {
                            Debug.LogWarning("Cheated on obtained item " + itemName);
                            cheated = true;
                        }
                    }


                    //Horrorlevel
                    if (data.horrorLevel != dataMeta.horrorLevel)
                    {
                        Debug.LogWarning("Cheated on horror level " + dataMeta.horrorLevel + "->" + data.horrorLevel);
                        cheated = true;
                    }
                }
                else
                {
                    Debug.LogWarning("CHEATED: Copied non-Meta in meta");
                    cheated = true;
                }
                
                streamMeta.Close();
            }
            else
            {
                Debug.LogWarning("CHEATED: deleted meta");
                cheated = true;
            }


            //!!! Sauvegarde meta est réalisée à partir de là

            ScenarioManager.Anger = anger; //new Dictionary<string, float>();
            ScenarioManager.flags = flags;// new Dictionary<string, bool>();
            ScenarioManager.HorrorLevel = data.horrorLevel;
            ScenarioManager.Inventory = inventory;
            /*
            //Anger
            for (int i = 0; i < Mathf.Min(data.ids.Length, data.angers.Length); i++)
            {
                ScenarioManager.Anger[data.ids[i]] = data.angers[i];
            }
            //Flags
            for (int i = 0; i < Mathf.Min(data.flagStrings.Length, data.flagStatuses.Length); i++)
            {
                ScenarioManager.flags[data.flagStrings[i]] = data.flagStatuses[i];
            }*/

            foreach (string s in ScenarioManager.flags.Keys)
            {
                Debug.Log(s + "/" + ScenarioManager.flags[s]);
            }

            if(cheated)
            {
                ScenarioManager.RaiseFlag(Flag.CHEATEDONCE);
                Debug.LogWarning("CHEATEDONCE");
            }
            //Place
            LocationManager.MoveTo(data.savePlace);
            stream.Close();

            Debug.Log("Loaded");
        }
        else
        {
            Debug.Log("No Save");
        }
        
    }
    public static Sprite GetSpriteSave(int saveNum)
    {
        string path = Path.Combine(Application.persistentDataPath, "Scenario" + saveNum + "-save.sav");

        if (!File.Exists(path))
        {
            return null;
        }
        FileStream stream = new FileStream(path, FileMode.Open);
        BinaryFormatter formatter = new BinaryFormatter();
        ScenarioData data = (ScenarioData)formatter.Deserialize(stream);

        Sprite sprite = Resources.Load<Sprite>("Locations/" + data.savePlace.ToString());

        stream.Close();
        return sprite;
        
    }
    /*
     Need a bool isSavingMeta to disallow meta save during loading
     */

}
