﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
public class LoadSave : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{

    [SerializeField] GameObject openCover;
    [SerializeField] GameObject closeCover;
    [SerializeField] GameObject screenShot;
    [SerializeField] Image screenShotImage;
    [SerializeField] GameObject saveText;

    [SerializeField] Sprite newSaveSprite;

    [SerializeField] float openSpeed;
    [SerializeField] float letterOutSpeed;

    [SerializeField] int saveNumber;
    float originalCoverHeight;
    bool isOpening;

    float originalOpenCoverY;
    float originalScreenShotY;
    // Start is called before the first frame update
    void Start()
    {
        originalCoverHeight= closeCover.GetComponent<RectTransform>().sizeDelta.y;
        originalOpenCoverY = openCover.GetComponent<RectTransform>().localPosition.y;//openCover.transform.position.y;
        originalScreenShotY = screenShot.GetComponent<RectTransform>().localPosition.y;


        Sprite saveSprite = Saver.GetSpriteSave(saveNumber);
        if (saveSprite != null)
        {
            screenShotImage.sprite = saveSprite;
        }
        else
        {
            screenShotImage.sprite = newSaveSprite;
            saveText.GetComponent<Text>().text = "New Save";
        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 openCoverSize = openCover.GetComponent<RectTransform>().sizeDelta;
        Vector2 closeCoverSize = closeCover.GetComponent<RectTransform>().sizeDelta;

        saveText.SetActive(isOpening);
        if (isOpening)
        {

            if (closeCoverSize.y>0)
            {
                closeCoverSize.y -= openSpeed * Time.deltaTime;
                closeCover.GetComponent<RectTransform>().sizeDelta = closeCoverSize;


                openCoverSize.y = 0;
                openCover.GetComponent<RectTransform>().sizeDelta = openCoverSize;
            }
            else if(openCoverSize.y < originalCoverHeight)
            {
                openCoverSize.y = Mathf.Min(openCoverSize.y + openSpeed * Time.deltaTime, originalCoverHeight);
                openCover.GetComponent<RectTransform>().sizeDelta = openCoverSize;

                Vector2 openPos = openCover.transform.localPosition;
                openCover.GetComponent<RectTransform>().localPosition = new Vector2(openPos.x, originalOpenCoverY + openCoverSize.y);

                closeCoverSize.y =0;
                closeCover.GetComponent<RectTransform>().sizeDelta = closeCoverSize;
            }
            else if(screenShot.transform.localPosition.y<originalCoverHeight+originalScreenShotY)
            {
                Vector3 screenShotPos = screenShot.transform.localPosition;
                screenShotPos.y = Mathf.Min(originalCoverHeight + originalScreenShotY, screenShotPos.y + letterOutSpeed * Time.deltaTime);
                screenShot.transform.localPosition = screenShotPos;
            }
        }
        else
        {
            if (screenShot.transform.localPosition.y > originalScreenShotY)
            {
                Vector3 screenShotPos = screenShot.transform.localPosition;
                screenShotPos.y = Mathf.Max(originalScreenShotY, screenShotPos.y - letterOutSpeed * Time.deltaTime);
                screenShot.transform.localPosition = screenShotPos;
            }
            else if (openCoverSize.y > 0)
            {
                openCoverSize.y = Mathf.Max(openCoverSize.y - openSpeed * Time.deltaTime, 0);
                openCover.GetComponent<RectTransform>().sizeDelta = openCoverSize;

                Vector2 openPos = openCover.transform.localPosition;
                openCover.GetComponent<RectTransform>().localPosition = new Vector2(openPos.x, originalOpenCoverY + openCoverSize.y);

                closeCoverSize.y = 0;
                closeCover.GetComponent<RectTransform>().sizeDelta = closeCoverSize;
            }

            else if (closeCoverSize.y < originalCoverHeight)
            {
                closeCoverSize.y = Mathf.Min(closeCoverSize.y + openSpeed * Time.deltaTime, originalCoverHeight);
                closeCover.GetComponent<RectTransform>().sizeDelta = closeCoverSize;

                openCoverSize.y = 0;
                openCover.GetComponent<RectTransform>().sizeDelta = openCoverSize;
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("Clooooooooooosing");
        isOpening = false;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("OOOOOOOOOOOpening");
        isOpening = true;
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("aaah");
        Saver.Load(saveNumber);
        SceneManager.LoadScene("Map_Fusion");
    }
}
