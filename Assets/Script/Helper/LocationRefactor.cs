﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationRefactor : MonoBehaviour
{

    
    // Start is called before the first frame update
    void Start()
    {
        Location[] locations = GetComponentsInChildren<Location>();
        foreach(Location location in locations)
        {
            GameObject go = new GameObject();
            go.transform.parent = transform;
            go.SetActive(true);
            go.transform.position=location.transform.position;
            go.name = location.name;
            location.transform.parent = go.transform;
            location.name = location.name + "Location";
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
