﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptyLocationManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        foreach(Transform tr in GetComponentsInChildren<Transform>())
        {
            if(tr.GetComponentsInChildren<Location>().Length==0 && tr.gameObject.GetInstanceID() != gameObject.GetInstanceID())
            {
                Destroy(tr.gameObject);
            }
        }
    }
}
