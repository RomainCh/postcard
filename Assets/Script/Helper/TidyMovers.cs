﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TidyMovers : MonoBehaviour
{
    [SerializeField] GameObject locationsManager;

    // Start is called before the first frame update
    void Start()
    {
        Move[] moves = GetComponentsInChildren<Move>();
        foreach(Move move in moves)
        {
            string[] stringSep = new string[] { "To" };
            Transform tr = locationsManager.transform.Find(Format(move.name.Split(stringSep, StringSplitOptions.RemoveEmptyEntries)[0]));
            if(tr!=null)
            {
                move.transform.SetParent(tr);
            }
        }
    }
    string Format(string s)
    {
        string s2="";
        foreach(char c in s)
        {
            if(Char.IsUpper(c) && s2!="")
            {
                s2 += " ";
            }
            s2 += c;
        }
        Debug.Log(s2);
        return s2;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
