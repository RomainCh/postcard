﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Initialization : MonoBehaviour
{
    [RuntimeInitializeOnLoadMethod()]
    static void Initialize()
    {
        Item[] items = Resources.LoadAll<Item>("Items");
        foreach(Item item in items)
        {
            //Debug.Log(item.name);
            Inventory.allItems.Add(item.nom, item);
            //ScenarioManager.Inventory.AddItem(item);
        }
    }

}
