﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraControl : MonoBehaviour {


    [SerializeField] bool freeMovements;

    public float speed;
    public float rot_sensibility;
    float m_x=0;
    float m_y=0;


    [SerializeField] string pathName;
    [SerializeField] bool saving = false;
    float saveTime;

    [SerializeField] PathWay path;

    [SerializeField] GameObject savingText;


    [SerializeField] GameObject locationText;
    [SerializeField] List<Location> locations;
    [SerializeField] GameObject locationManager;

    [Tooltip("The higher it is, the faster the camera will move when changing direction.")]
    [SerializeField] float lookRatio;

    int currentLocationIndex;

    Vector3 targetRotation = new Vector3();

    // Use this for initialization
    void Start () {
        m_x = Input.mousePosition.x;
        m_y = Input.mousePosition.y;
        targetRotation = transform.eulerAngles;
        //Cursor.visible = false;

        /*
        List<Location> locationsInv = new List<Location>();
        for (int i=0;i<locations.Count;i++)
        {
            locationsInv.Add(locations[locations.Count - 1 - i]);
        }
        locations = locationsInv;
        */
        Location[] locationsArray = locationManager.GetComponentsInChildren<Location>();
        locations = new List<Location>();
        foreach(Location location in locationsArray)
        {
            locations.Add(location);
        }
        pathName = locations[currentLocationIndex].place.ToString() + "To" + locations[currentLocationIndex + 1].place.ToString();
        locationText.GetComponent<Text>().text = locations[currentLocationIndex].GetComponent<Location>().place.ToString();
    }

    // Update is called once per frame
    void Update ()
    {
        

        if(freeMovements)
        {
            savingText.gameObject.SetActive(saving);

            float dx = 0;
            float dy = 0;
            float dz = 0;
            //m_MouseLook.Init(transform, Camera.main.transform);
            if (Input.GetKey(KeyCode.Z))
            {
                dy--;
            }
            if (Input.GetKey(KeyCode.S))
            {
                dy++;
            }
            if (Input.GetKey(KeyCode.Q))
            {
                dx++;
            }
            if (Input.GetKey(KeyCode.D))
            {
                dx--;
            }
            if (Input.GetKey(KeyCode.Space))
            {
                dz--;
            }
            if (Input.GetKey(KeyCode.LeftControl))
            {
                dz++;
            }

            if (Input.GetKey(KeyCode.KeypadPlus))
            {
                speed *= 1.1f;
            }
            if (Input.GetKey(KeyCode.KeypadMinus))
            {
                speed /= 1.1f;
            }

            if (Input.GetKey(KeyCode.KeypadMultiply))
            {
                rot_sensibility *= 1.1f;
            }
            if (Input.GetKey(KeyCode.KeypadDivide))
            {
                rot_sensibility /= 1.1f;
            }
            if (Input.GetKey(KeyCode.Alpha0))
            {
                rot_sensibility =0;
            }
            if (Input.GetKey(KeyCode.Alpha1))
            {
                rot_sensibility = 1;
            }

            if(Input.GetKeyDown(KeyCode.F3))
            {
                
                
            }
            transform.position -= (transform.forward * dy * speed + transform.right * dx * speed + transform.up * dz * speed) * 60 * Time.deltaTime;

            
            float r_x = Input.GetAxis("Mouse X");
            float r_y = Input.GetAxis("Mouse Y");
            /*
            float r_x = 0;
            float r_y = 0;*/
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                r_x--;
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                r_x++;
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                r_y++;
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                r_y--;
            }

            Vector3 rot = transform.eulerAngles;

            transform.rotation = Quaternion.Euler(rot.x - rot_sensibility * r_y/3, rot.y + rot_sensibility * r_x, 0);

            if (Input.mousePosition.x >= Screen.width * 0.9 || Input.mousePosition.x <= 1 || Input.mousePosition.y >= Screen.height * 0.9 || Input.mousePosition.y <= 1)
            {
                Cursor.SetCursor(null, new Vector2(Screen.width / 2, Screen.height / 2), CursorMode.Auto);
            }
            m_x = Input.mousePosition.x;
            m_y = Input.mousePosition.y;
            Cursor.visible = false;
            


            saveTime += Time.deltaTime;

            //Saving
            if (saving)
            {
                path.AddTransformation(transform, saveTime);
            }
            if (Input.GetMouseButtonDown(1))
            {
                if (!saving)
                {
                    path = new PathWay();
                    saving = true;
                    saveTime = 0;
                    if (currentLocationIndex < locations.Count)
                    {
                        //locations[currentLocationIndex].transform.position = transform.position;
                        //locations[currentLocationIndex].transform.rotation = transform.rotation;

                        locationText.GetComponent<Text>().text = locations[currentLocationIndex].GetComponent<Location>().place.ToString()
                            + "To" + locations[currentLocationIndex+1].GetComponent<Location>().place.ToString();
                    }
                }
                else
                {

                    JsonSerialization.WriteToJsonResource("Paths/" + pathName, path);

                    if (currentLocationIndex < locations.Count)
                    {
                        currentLocationIndex++;
                    }
                    if (currentLocationIndex < locations.Count)
                    {
                        pathName = locations[currentLocationIndex].place.ToString() + "To" + locations[currentLocationIndex + 1].place.ToString();
                        locations[currentLocationIndex].transform.position = transform.position;//
                        locations[currentLocationIndex].transform.rotation = transform.rotation;//
                    }

                    saving = false;
                    locationText.GetComponent<Text>().text = locations[currentLocationIndex].GetComponent<Location>().place.ToString();

                }
                
            }


            if(Input.GetKeyDown(KeyCode.Backspace))
            {
                saving = false;
                currentLocationIndex=Mathf.Max(0,currentLocationIndex-1);

                transform.position = locations[currentLocationIndex].transform.position;
                transform.rotation = locations[currentLocationIndex].transform.rotation;

                locationText.GetComponent<Text>().text = locations[currentLocationIndex].GetComponent<Location>().place.ToString();

            }

        }

        else
        {
            float r_x = 100 * (Input.mousePosition.x - m_x) / Screen.width;
            float r_y = 100 * (Input.mousePosition.y - m_y) / Screen.height;
            Vector3 rot = transform.eulerAngles;

            targetRotation = new Vector3(targetRotation.x - rot_sensibility * r_y, targetRotation.y + rot_sensibility * r_x, 0);

            targetRotation = CameraFollow.LowestDifferenceAngle(targetRotation, transform.rotation.eulerAngles);

            transform.rotation = Quaternion.Euler(targetRotation * lookRatio + transform.rotation.eulerAngles*(1- lookRatio));


            //transform.rotation = Quaternion.Euler(rot.x - rot_sensibility * r_y, rot.y + rot_sensibility * r_x, 0);

            m_x = Input.mousePosition.x;
            m_y = Input.mousePosition.y;
        }

        
        
        //
    }

    public void ResetMouse()
    {
        m_x = Screen.width/2;
        m_y = Screen.height / 2;
        targetRotation = transform.eulerAngles;
    }
}
