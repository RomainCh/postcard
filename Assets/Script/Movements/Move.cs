﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Move : MonoBehaviour
{

    [SerializeField] float displayDistance;//Max distance between mouse and object to display the object

    [SerializeField] Place place;//place to move to when clicked
    private static bool displayAll;//Do we display all the movement objects ?

    [SerializeField] string indication;
    [SerializeField] Text indicationText;

    //Components
    SpriteRenderer spriteRenderer;

    Location location;

    // Start is called before the first frame update
    void Start()
    {

        /*Change name*/
        location = transform.parent.GetComponentInChildren<Location>();

        gameObject.name = location.place.ToString() + "To" + place.ToString();

        spriteRenderer = GetComponent<SpriteRenderer>();

        spriteRenderer.enabled = false;
        displayAll = false;


        if (indicationText == null)
        {
            indicationText = DialogDisplayer.instance.mouseText;
        }
        if (indication == "")
        {
            indication = "To " + place.ToString();
        }
    }

    
    private void OnMouseUp()
    {
        if(IsEnabled())
        {
            LocationManager.MoveTo(place);
        }
    }


    public void OnMouseOver()
    {
        
        if (IsEnabled() && Controller.MouseAvailableWorld())
        {
            spriteRenderer.enabled=true;
            Vector3 mousePos = Input.mousePosition;
            indicationText.text = indication;
            indicationText.transform.position = new Vector3(mousePos.x, mousePos.y, mousePos.z);
            indicationText.gameObject.SetActive(true);
            if (Controller.WorldMouseButtonDown(0))
            {
                LocationManager.MoveTo(place);
            }
        }
    }

    public void OnMouseExit()
    {
        spriteRenderer.enabled = false;
        indicationText.gameObject.SetActive(false);
    }

    public bool IsEnabled()
    {
        return LocationManager.currentLocation.place == location.place;
    }
}
