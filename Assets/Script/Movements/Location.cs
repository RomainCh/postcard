﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;




[Serializable]
public class Location : MonoBehaviour
{
    public Place place;
    [HideInInspector]
    public Sprite sprite;
    public List<Flag> flagsToRaise;
    public List<Flag> flagsToDrop;

    void Start()
    {
        if(sprite==null)
        {
            //sprite = GetComponent<SpriteRenderer>().sprite;
            sprite = Resources.Load<Sprite>(place.ToString());
        }
       
    }
}
[Serializable]
public enum Place
{
    Bookshop,
    House1,
    House2,
    House3,
    House4,
    House5,
    House6,
    House7,
    House8,
    House9,
    House10,
    House11,
    House12,
    House13,
    House14,
    GroceryStore,
    Store3,
    PostOffice,
    EastBridge,
    WestBridge,
    Church,
    Farm
};
