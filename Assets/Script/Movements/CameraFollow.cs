﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    [SerializeField] TextAsset pathAsset;
    [SerializeField] float ratio;
    [SerializeField] bool following;
    [SerializeField] bool capturing;
    float currentTime=0;
    public PathWay path;
    int currentIndex;

    bool ajustToLocation=true;

    [SerializeField] float timeToAdapt;

    Transformation transformationToAdapt;

    // Start is called before the first frame update
    void Start()
    {
        //path = JsonSerialization.ReadFromJsonResource<PathWay>(pathAsset);
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(currentTime);
        
        if(following)
        {
            currentTime += Time.deltaTime * ratio;
            while (currentIndex < path.transformations.Count && 
                path.transformations[currentIndex].time <= currentTime)
            {
                transform.position = path.transformations[currentIndex].position;
                transform.rotation = path.transformations[currentIndex].rotation;
                currentIndex++;

                
            }
            if(currentIndex>=path.transformations.Count)
            {
                if(ajustToLocation)
                {
                    if (currentTime - transformationToAdapt.time <= timeToAdapt * ratio)
                    {
                        float adaptRatio = (currentTime - transformationToAdapt.time) / (timeToAdapt * ratio);

                        transform.position = transformationToAdapt.position * (1 - adaptRatio) +
                            LocationManager.currentLocation.transform.position * adaptRatio;


                        Vector3 angleToAdapt = transformationToAdapt.rotation.eulerAngles;
                        Vector3 targetAngle = LocationManager.currentLocation.transform.rotation.eulerAngles;

                        targetAngle = LowestDifferenceAngle(targetAngle, angleToAdapt);

                        transform.rotation = Quaternion.Euler(angleToAdapt * (1 - adaptRatio) +
                                targetAngle * adaptRatio);

                        //transform.rotation = LocationManager.currentLocation.transform.rotation;
                    }
                    else
                    {
                        following = false;
                        currentIndex = 0;
                        transform.position = LocationManager.currentLocation.transform.position;
                        transform.rotation = LocationManager.currentLocation.transform.rotation;

                        Controller.ResetItem();
                        //Cursor.SetCursor(Controller.baseCursor, new Vector2(Screen.width / 2, Screen.height / 2), CursorMode.Auto);
                        GetComponent<CameraControl>().ResetMouse();

                        if(capturing)
                        {
                            ScreenCapture.CaptureScreenshot("Assets/Resources/Locations/" + LocationManager.currentLocation.place.ToString() + ".png");
                        }

                    }
                }
                else
                {
                    following = false;
                    currentIndex = 0;
                }
                
            }

        }
    }

    public void Follow(PathWay path)
    {
        
        this.path = path;
        currentIndex = 0;
        Debug.Log("Path length:" + path.transformations.Count);
        following = true;
        ajustToLocation = true;

        transformationToAdapt = path.transformations[path.transformations.Count - 1];
        currentTime = 0;
    }


    public static Vector3 LowestDifferenceAngle(Vector3 anglesToChange, Vector3 reference)
    {

        while (anglesToChange.x > 180 + reference.x)
        {
            anglesToChange.x -= 360;
        }
        while (anglesToChange.x < reference.x - 180)
        {
            anglesToChange.x += 360;
        }
        while (anglesToChange.y > 180 + reference.y)
        {
            anglesToChange.y -= 360;
        }
        while (anglesToChange.y < reference.y - 180)
        {
            anglesToChange.y += 360;
        }
        while (anglesToChange.z > 180 + reference.z)
        {
            anglesToChange.z -= 360;
        }
        while (anglesToChange.z < reference.z - 180)
        {
            anglesToChange.z += 360;
        }
        return anglesToChange;
    }
}
