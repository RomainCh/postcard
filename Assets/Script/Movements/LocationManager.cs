﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;


/*
[Serializable]
public enum Place
{
    CYBERPUNK,
    MANOIR,
    TRAMWAY
};

[Serializable]
public struct Location
{

    public Place place;
    public SpriteRenderer backgroundSprite;
    public Transform transform;
    public Sprite sprite;
    public List<Flag> flagsToRaise;
    public List<Flag> flagsToDrop;
    

}*/

public class LocationManager : MonoBehaviour
{
    //Only one at a given time
    public static LocationManager instance;

    static bool movedFromInitialLocation = false;

    [SerializeField] CameraFollow cameraFollow;

    [Tooltip("Which place to start in")]
    [SerializeField] Place startPlace;

    [Tooltip("All locations this manager is responsible for")]
    //[SerializeField] List<Location> locations;
    Location[] locations;
    //Gets the location struct from a given place
    public static Dictionary<Place, Location> locationFromPlace = new Dictionary<Place, Location>();

    //Location currently on screen
    public static Location currentLocation;

    //Sprite currently on screen
    public static Sprite currentSprite;

    public static string scene;
    void Awake()
    {
        locations = GetComponentsInChildren<Location>();
        scene = SceneManager.GetActiveScene().name;
        foreach (Location location in locations)
        {
            locationFromPlace[location.place] = location;
        }

        if (instance != null && instance != this)
        {
            Destroy(instance.gameObject);
        }
        instance = this;
        
        if(!movedFromInitialLocation)
        {
            MoveTo(startPlace);
        }
        else
        {
            MoveTo(currentLocation.place);
        }
    }


    public static void MoveTo(Place place)
    {
        if(currentLocation!=null && place==currentLocation.place)
        {
            return;
        }

        try
        {
            Debug.Log("FROM: " + currentLocation.place + "TO: " + place);
        }
        catch
        {

        }

        Location newLocation = locationFromPlace[place];
        Debug.Log(newLocation.place);
        Vector3 locationPosition = newLocation.transform.position;
        Quaternion locationRotation = newLocation.transform.rotation;
        TextAsset pathAsset = null;

        if(movedFromInitialLocation)
        {
            try
            {
                pathAsset = Resources.Load<TextAsset>("Paths/" + currentLocation.place.ToString() +
                "To" + place.ToString());
                Debug.Log("FROM: " + currentLocation.place + "TO: " + place);
            }
            catch
            {
                Debug.LogWarning(currentLocation.place.ToString() + "To" + place.ToString() + "not found");
            }
        }

        currentLocation = newLocation;
        currentSprite = currentLocation.sprite;

        if (pathAsset != null && instance.cameraFollow != null)
        {
            PathWay path = JsonSerialization.ReadFromJsonResource<PathWay>(pathAsset);
            
            instance.cameraFollow.Follow(path);
        }
        else if(instance.cameraFollow!=null)
        {
            instance.cameraFollow.transform.position = locationPosition;
            instance.cameraFollow.transform.rotation = locationRotation;
        }
        



        movedFromInitialLocation = true;
        foreach (Flag flag in newLocation.flagsToRaise)
        {
            ScenarioManager.RaiseFlag(flag);
        }
        foreach (Flag flag in newLocation.flagsToDrop)
        {
            ScenarioManager.DropFlag(flag);
        }
    }
}
