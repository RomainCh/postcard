﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public struct Transformation
{
    [SerializeField] public Vector3 position;
    [SerializeField] public Quaternion rotation;
    [SerializeField] public float time;

    public Transformation(Transform transform, float time)
    {
        position = transform.position;
        rotation = transform.rotation;
        this.time = time;
    }
}
[Serializable]
public class PathWay
{
   [SerializeField]  public List<Transformation> transformations = new List<Transformation>();
    public void AddTransformation(Transform transform, float time)
    {
        transformations.Add(new Transformation(transform, time));
    }
}
