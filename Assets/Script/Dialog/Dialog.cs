﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Dialog
{
    [SerializeField]
    public List<DialogLine> lines = new List<DialogLine>();
}
