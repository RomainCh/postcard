﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public struct NextLine
{

    public int id;

    public List<Flag> raisedFlagConditions;
    public List<Flag> droppedFlagConditions;

    public List<string> itemsToHave;
    public List<string> itemsNotToHave;


    public int minHorrorLevel;
    public int maxHorrorLevel;
    


}

[Serializable]
public struct DialogLine
{
    public int id;
    [TextArea(5,10)]
    public string text;

    public List<NextLine> nextLines;

    public List<Flag> flagsToRaise;
    public List<Flag> flagsToDrop;

    public List<string> itemsToGet;
    public List<string> itemsToLose;

    public int defaultNextId;

    public DialogLine(int id)
    {
        this.id = id;
        text = "";
        nextLines = new List<NextLine>();
        flagsToRaise = new List<Flag>();
        flagsToDrop = new List<Flag>();
        itemsToGet = new List<string>();
        itemsToLose = new List<string>();
        defaultNextId = -1;
    }
    public int GetNextLine()
    {
         
        foreach(NextLine nextLine in nextLines)
        {
            bool condition = true;
            foreach(Flag flag in nextLine.raisedFlagConditions)
            {
                if(!ScenarioManager.IsRaised(flag))
                {
                    condition = false;
                }
            }
            if(condition)
            {
                foreach (Flag flag in nextLine.droppedFlagConditions)
                {
                    if (ScenarioManager.IsRaised(flag))
                    {
                        condition = false;
                    }
                }
            }
            if(condition)
            {
                foreach(string itemName in nextLine.itemsToHave)
                {
                    if(!ScenarioManager.Inventory.HasItem(itemName))
                    {
                        condition = false;
                    }
                }
                foreach (string itemName in nextLine.itemsNotToHave)
                {
                    if (ScenarioManager.Inventory.HasItem(itemName))
                    {
                        condition = false;
                    }
                }
            }
            if(condition && ScenarioManager.HorrorLevel>=nextLine.minHorrorLevel 
                && ScenarioManager.HorrorLevel <= nextLine.maxHorrorLevel)
            {
                return nextLine.id;
            }
        }
        return defaultNextId;
    }

}
