﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnswersDisplayer : MonoBehaviour
{
    public static AnswersDisplayer instance;

    [SerializeField]
    GameObject answerFrame;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;

        answerFrame.SetActive(false);
    }

    // Update is called once per frame
    public void DisplayAnswers(List<string> answers)
    {
        foreach (AnswerText answer in GetComponentsInChildren<AnswerText>(true))
        {
            answer.UpdateAnswer(answers);
        }
        answerFrame.SetActive(true);
    }

    public void ChooseAnswer(int answerIndex)
    {
        answerFrame.SetActive(false);
        DialogDisplayer.instance.Answer(answerIndex);
    }
}
