﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SilouhetteObject : MonoBehaviour
{
    [SerializeField]
    GameObject img;

    [SerializeField]
    float speed = 30;

    int nbCharacters = 0;
    SpriteRenderer spriteRenderer;

    float compt = 0;

    Vector3 targetPosition;
    bool isMoving = false;

    private void Awake()
    {
        spriteRenderer = img.GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (isMoving)
        {
            float distance = targetPosition.x - transform.position.x;
            compt += speed * Time.deltaTime;
            float step = RegulateSpeed(distance, compt); // calculate distance to move
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);

            if (Vector3.Distance(transform.position, targetPosition) < 0.001f)
            {
                isMoving = false;
     
            }
        }
    }

    private float RegulateSpeed(float distance, float absX)
    {
        Debug.Log(distance * Mathf.Sin(absX / Mathf.PI));
        float val = distance * Mathf.Sin(absX / Mathf.PI);
        if (val > 0) {
            return distance * Mathf.Sin(absX / Mathf.PI);
        }
        else
        {
            return speed * Time.deltaTime;
        }
    }

    public void InitPosition(Vector2 pos) {
        gameObject.transform.position = (Vector3) pos;
    }

    public void Movement(Vector3 endPosition)
    {
        compt = 0;
        isMoving = true;
        spriteRenderer.flipX = !spriteRenderer.flipX;
        targetPosition = (Vector3) endPosition;
    }

}
