﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartDialog : MonoBehaviour
{
    [SerializeField]
    private DialogDisplayer dialogMenu;

    [SerializeField]
    private GameObject character;

    private List<GameObject> listCharacters = new List<GameObject>();

    static StartDialog instance;
    void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        dialogMenu = DialogDisplayer.instance;
    }

    public void LoadDialog(TextAsset inkAsset)
    {
        dialogMenu.BeginDialog(inkAsset);
    }

    /*
    public static void LoadDialog(string s)
    {
        Dialog dialog = new Dialog();
        DialogLine line = new DialogLine(0);
        
        line.text = s;
        line.defaultNextId = -1;
        dialog.lines = new List <DialogLine>{line};
        DialogDisplayer.SetDialog(dialog);
        instance.dialogMenu.SetActive(true);
        //DialogDisplayer.BeginDialog("aaaa\nbbbbbb");
        DialogDisplayer.BeginDialog();
        //AddCharacter();
    }*/

    public void AddCharacter()
    {
        PositionCharacter(new Vector3(0.8f, 1, 100));
    }

    public void PositionCharacter(Vector3 pos)
    {
        if (listCharacters.Count>0)
        {
            listCharacters[listCharacters.Count-1].GetComponent<SilouhetteObject>().Movement(new Vector2(6-(listCharacters.Count-1)*0.3f, 0));
            pos.x = -6;
        }

        GameObject temp = Instantiate(character, pos, Quaternion.identity);
        temp.transform.SetParent(dialogMenu.transform, false);
        listCharacters.Add(temp);
    }
}
