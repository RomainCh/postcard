﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AnswerText : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    [SerializeField]
    int answerIndex;
    [SerializeField]
    Color selectedColor = new Color(1,0.7f,0.4f);


    bool selected = false;

    Text text;
    void Awake()
    {
        text = GetComponent<Text>();
    }
    void Update()
    {
        if (selected)
        {
            text.color = selectedColor;
            if (Input.GetMouseButtonDown(0))
            {
                //Answer
                AnswersDisplayer.instance.ChooseAnswer(answerIndex);
            }
        }
        else
        {
            text.color = Color.white;
        }
    }
    // Start is called before the first frame update
    public void OnPointerEnter(PointerEventData eventData)
    {
        selected = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        selected = false;
    }

    public void UpdateAnswer(List<string> answers)
    {
        text = GetComponent<Text>();
        if (answers.Count>answerIndex)
        {
            this.enabled = true;
            text.text = answers[answerIndex];
        }
        else
        {
            text.text = "";
            this.enabled = false;
        }

    }
}
