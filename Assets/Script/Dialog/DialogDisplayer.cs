﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Ink.Runtime;
using UnityEngine.SceneManagement;

public class DialogDisplayer : MonoBehaviour
{

    public static DialogDisplayer instance;

    [Header("Ink content")]
    Story story;

    [Header("Dialog display")]
    [SerializeField] GameObject child;

    [SerializeField]
    Text textUI;

    [SerializeField]
    Text speakerUI;

    [SerializeField]
    float timeBetweenCharacters;

    [SerializeField]
    float ratioClick;

    [SerializeField]
    int maxNumberOfLines;



    public Text mouseText;

    public static Story staticStory;


    bool dialogEnded=false;

    int currentLine = 0;


    void Awake()
    {
        instance = this;

        child.SetActive(false);
    }

    void Update()
    {
        if (dialogEnded && Input.GetMouseButtonDown(0))
        {
            ContinueDialog();
        }
        if(!child.activeInHierarchy && Input.GetMouseButtonUp(0))
        {
            Controller.dialogOpen = false;
        }
    }

    public void BeginInteraction(TextAsset inkAsset, string inkPath = "")
    {
        if(Controller.item==null)
        {
            BeginDialog(inkAsset, inkPath + ".interact");
        }

        else
        {
            Story tempStory = new Story(inkAsset.text);
            string pathWithItem = inkPath + ".interact_with_" + FormatStringForInk(Controller.item.nom);

            BeginDialog(inkAsset, pathWithItem);
            Controller.ResetItem();
        }
    }

    public void BeginDialog(TextAsset inkAsset, string inkPath = "")
    {
        StopAllCoroutines();
        Controller.dialogOpen = true;
        child.SetActive(true);
        story = new Story(inkAsset.text);

        story.BindExternalFunction("GetFlag", (string arg1) => {
            return ScenarioManager.IsRaised(arg1);
        });
        story.BindExternalFunction("GetItem", (string arg1) => {
            return ScenarioManager.Inventory.HasItem(arg1);
        });
        story.BindExternalFunction("HorrorLevel", () => {
            return ScenarioManager.HorrorLevel;
        });

        if (inkPath != "")
        {
            story.ChoosePathString(inkPath);
        }

        ContinueDialog();
    }
    public void ContinueDialog()
    {
        instance.dialogEnded = false;
        if(story.canContinue)
        {
            StartCoroutine(ContinueSaying(textUI));
        }
        else
        {
            if (story.currentChoices.Count > 0)
            {
                List<string> answers = new List<string>();
                for (int i = 0; i < story.currentChoices.Count; i++)
                {
                    answers.Add(story.currentChoices[i].text);
                }
                AnswersDisplayer.instance.DisplayAnswers(answers);
            }
            else
            {
                child.SetActive(false);
            }
        }
    }

    IEnumerator ContinueSaying(Text textBox)
    {
        //Text display
        string text = story.Continue();
        
        string speakerName = text.CharacterTalking();
        if (speakerName != "") {
            speakerUI.transform.parent.gameObject.SetActive(true);
            speakerUI.text = speakerName;

            text = text.ActualText();
        } else
        {
            speakerUI.transform.parent.gameObject.SetActive(false);
        }
        
        List<string> tags = story.currentTags;
        foreach (string tag in tags)
        {
            TagParse(tag);
        }

        string endTags;

        for (int i = 0; i <= text.Length; i++)
        {
            
            textBox.text = text.Substring(0, i);


            string[] lines = textBox.text.Split('\n');
            Sounds.Play("sound_dialog_char");
            if (lines.Length > maxNumberOfLines)
            {
                string newText = "";
                for(int j=maxNumberOfLines;j>1;j--)
                {
                    newText += lines[lines.Length - j] + "\n";
                }
                newText += lines[lines.Length - 1];
                textBox.text = newText;
            }
            float waitTime = timeBetweenCharacters;
            if(Input.GetMouseButton(0))
            {
                waitTime*=ratioClick;
            }
            yield return new WaitForSeconds(waitTime);
        }

        //Get to next dialog line
        yield return new WaitForSeconds(timeBetweenCharacters*2);
        dialogEnded = true;
    }

    public void Answer(int index)
    {
        story.ChooseChoiceIndex(index);
        ContinueDialog();
    }

    void TagParse(string tag)
    {
        string argument;
        string order = tag.ExtractArgument(out argument);
        
        switch(order)
        {
            case "RaiseFlag":
                ScenarioManager.RaiseFlag(argument);
                break;
            case "DropFlag":
                ScenarioManager.DropFlag(argument);
                break;
            case "AddItem":
                ScenarioManager.Inventory.AddItem(argument);
                break;
            case "DeleteItem":
                ScenarioManager.Inventory.DeleteItem(argument);
                break;
            case "PlayBGM":
                MusicManager.Instance.PlayBGM(argument);
                break;
            case "Puzzle":
                SceneManager.LoadScene(argument);
                break;
            default:
                Debug.LogError("invalid tag : " + tag);
                break;
        }
    }



    static bool HasPath(Story story, string path)
    {
        try
        {
            story.ChoosePathString(path);
        }
        catch (Exception e)
        {
            return false;
        }

        return true;
    }

    public static string FormatStringForInk(string str)
    {
        return str.Replace(" ", "_").ToLower();
    }

}


public static class stringExtension
{
    public static string CharacterTalking(this string text)
    {
        //returns the person talking if there is any, modifies text to take out the character's name
        int index = text.IndexOf(":");

        if (index == -1) { return ""; }

        string name = "";
        name = text.Substring(0, index);
        name.Trim();

        return name;
    }

    public static string ActualText(this string text)
    {
        int index = text.IndexOf(":");

        if (index == -1) { return text; }

        string actualText = "";
        actualText = text.Substring(index + 1);
        actualText.Trim();

        return actualText;
    }


    public static string ExtractArgument(this string tag, out string argument)
    {
        argument = "";
        int index = tag.IndexOf("_");

        if(index == -1) { Debug.LogError("invalid tag : " + tag); return ""; }

        string order = "";
        order = tag.Substring(0, index);

        argument = tag.Substring(index + 1);
        argument = argument.Replace("_", " ");

        return order;
    }


    
}