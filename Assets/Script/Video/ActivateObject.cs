﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateObject : MonoBehaviour
{

    public GameObject spawn;
    [SerializeField] float time;
    [SerializeField] float speed = 1f;
    

    // Start is called before the first frame update
    void Start()
    {
        Invoke("Spawn", time);    
    }
    void Update()
    {
        transform.Translate(Vector3.up * Time.deltaTime * speed);
    }
    void Spawn()
    {
        spawn.SetActive(true);
        gameObject.SetActive(false);
    }
}
