﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apparition : MonoBehaviour
{

    [SerializeField] GameObject village;
    [SerializeField] GameObject spawner;


    List<GameObject> rootObjects = new List<GameObject>();

    [SerializeField] float minTime;
    [SerializeField] float maxTime;
    float apparitionTime;
    float apparitionTimer;
    int currentObjectIndex;

    [SerializeField]  int numObjectsToSpawn=1;


    bool activated = false;
    // Start is called before the first frame update
    void Start()
    {
        currentObjectIndex = 0;
        apparitionTimer = 0;
        rootObjects = new List<GameObject>();
        foreach (Transform tr in village.GetComponentsInChildren<Transform>())
        {
            bool hasChild = false;
            if(tr.GetComponent<Renderer>() != null)//true || !hasChild)
            {
                tr.gameObject.SetActive(false);
                rootObjects.Add(tr.gameObject);
            }   
        }

        //drootObjects = Permute(rootObjects);
    }

    // Update is called once per frame
    void Update()
    {

        if(Input.GetKeyDown(KeyCode.Return))
        {
            activated = !activated;
        }
        if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            numObjectsToSpawn=Mathf.Max(0, numObjectsToSpawn-1);
        }
        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            numObjectsToSpawn++;
        }
        if(Input.GetKeyDown(KeyCode.R))
        {
            Start();
        }

        if(activated)
        {
            if (currentObjectIndex < rootObjects.Count)
            {
                apparitionTimer += Time.deltaTime;
                if (apparitionTimer >= apparitionTime)
                {
                    for (int i = 0; i < numObjectsToSpawn; i++)
                    {
                        if (currentObjectIndex < rootObjects.Count)
                        {
                            apparitionTimer = 0;
                            apparitionTime = Random.Range(minTime, maxTime);

                            GameObject newSpawner = Instantiate(spawner);
                            newSpawner.GetComponent<ActivateObject>().spawn = rootObjects[currentObjectIndex];
                            newSpawner.transform.position = rootObjects[currentObjectIndex].transform.position;
                            newSpawner.SetActive(true);
                            //rootObjects[currentObjectIndex].SetActive(true);
                            currentObjectIndex++;
                        }
                    }

                }
            }
        }
        
        
    }


    List<T> Permute<T>(List<T> list)
    {
        for (int i = 0; i <= list.Count - 2; i++)
        {
            int j = Random.Range(i, list.Count);

            T temp = list[i];
            list[i] = list[j];
            list[j] = temp;
        }
        return list;
    }

}
