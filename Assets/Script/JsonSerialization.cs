﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class JsonSerialization
{
    public static T ReadFromJsonResource<T>(TextAsset ta)
    {
        T obj = JsonUtility.FromJson<T>(ta.text);
        return obj;
    }
    public static void WriteToJsonResource<T>(string filePath, T obj, bool append = false)
    {
        TextWriter writer = null;

        if (!Directory.Exists(Path.Combine(Application.dataPath, "Resources")))
        {
            Directory.CreateDirectory(Path.Combine(Application.dataPath, "Resources"));
        }
        try
        {
            string json = JsonUtility.ToJson(obj,true);
            writer = new StreamWriter(Path.Combine(Path.Combine(Application.dataPath, "Resources"), filePath + ".json"), append);
            writer.Write(json);
        }
        finally
        {
            if (writer != null)
                writer.Close();
            Debug.Log("json serialized into " + filePath + ".json");
        }
    }
}
