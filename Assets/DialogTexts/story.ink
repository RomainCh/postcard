EXTERNAL GetFlag(flag)
EXTERNAL GetItem(item)
EXTERNAL HorrorLevel()

-> book_guy.interact

=== book_guy ===
-> interact_with_default

= interact
{GetItem("Cursed heart"): Book Giver: I'll give you a book for that heart. -> END}
{GetItem("Unknown book"): Book Giver: I hope you like your book. -> END}
Book Giver: Do you want a book? #PlayBGM_Tension
+ [Yes] Here, take this book free of charge! #AddItem_Unknown_book #PlayBGM_Normal
+ [No, I'd rather do a puzzle!] As you wish. #Puzzle_Sceal
- -> END

= interact_with_cursed_heart
Book Giver: As promised: a book for your heart! #DeleteItem_Cursed_heart #AddItem_Unknown_book
-> END

= interact_with_default
Book Giver: I have no further business with you and that... thing. 
-> END



=== heart_guy ===
-> interact_with_default

= interact
{GetItem("Unknown book"): Heart Giver: I'll give you my heart for that book. -> END}
{GetItem("Cursed heart"): Heart Giver: I hope you like my love. -> END}
Heart Giver: Do you wish to take my heart?
+ [Yes] The last person who had it will live on in your heart! #AddItem_Cursed_heart
+ [No] What a pity...
- -> END

= interact_with_unknown_book
Heart Giver: As promised: my heart for that book! #DeleteItem_Unknown_book #AddItem_Cursed_heart
-> END

= interact_with_default
Heart Giver: I don't ever want you to forget me... EVER!
-> END




=== manoir ===
Welcome to the manoir! 
{GetItem("Cursed heart"): -> GetHeart} 
{not GetItem("Cursed heart"): -> LoseHeart}


=== door ===
This is a door !
->END


=== GetHeart ===
    I will keep your heart with me for now. #DeleteItem_Cursed_heart
    ->END

=== LoseHeart ===
    Here, take your heart back. #AddItem_Cursed_heart
- -> END

=== london ===
- I looked at Monsieur Fogg 
and looked      #RaiseFlag_CHEATEDONCE
*   ... and I could contain myself no longer.
    Little shit: 'What is the purpose of our journey, Monsieur?'
    Fogg: 'A wager,' he replied.
    * *     choice 1
    **  chaoice 2
    * * choice A
    - - end choice

* ... and I didn't give a damn
    Little shit: 'A wager!' I returned.
    He nodded. 
    Little shit: 'But surely that is foolishness!'
    He nodded again.
    Little shit: 'But can we win?'
    Fogg: 'That is what we will endeavour to find out,' he answered.
- we passed the day in silence.


- -> END



=== items ===
-> not_found

= not_found
I have no idea what the hell that is ...
-> END

= cursed_heart
A heart whose owner is rumored to be looking for...
-> END